document.addEventListener("DOMContentLoaded", function () {
  console.log("DOM fully loaded and parsed");

  const myCanvas = document.getElementById("mycanvas");
  myCanvas.width = innerWidth;
  myCanvas.height = innerHeight;
  myCanvas.style.backgroundColor = "purple";
  console.log("🚀 ~ myCanvas:", myCanvas);

  const c = myCanvas.getContext("2d");
  console.log("🚀 ~ c:", c);

  class Silhouette {
    constructor(x, y, radius, color) {
      this.x = x;
      this.y = y;
      this.radius = radius;
      this.color = color;
    }
  }

  class Player {
    constructor() {
      this.position = {
        x: 10,
        y: 10,
        //    xx: this.x,
        //    yy: this.y,
      };

      //  this.width = ww;
      //  this.height = hh;
      this.width = 30;
      this.height = 30;
      //  this.color = "red";

      //  this.x = x;
      //  this.y = y;
      //  this.radius = radius;
      //  this.color = color;
    }

    velocity() {
      this.velocity = {
        x: 0,
        y: 200,
      };
    }

    update() {
      // this.position.x += this.velocity.x;
      this.position.y += this.velocity.y;
      this.draw();
    }

    draw() {
      c.fillStyle = "red";

      //  c.fillRect(x, y, w, h);
      c.fillRect(this.position.x, this.position.y, this.width, this.height);

      // c.fillStyle( this.color );
    }

    drawMe() {
      //  c.fillRect(x, y, w, h);
      c.fillStyle("red");

      c.fillRect(this.position.x, this.position.y, this.width, this.height);
    }
  }

  const player = new Player();
  player.draw();
  player.update()     //  player.draw();


  function animate() {


     
  }
  //   c.fillStyle= "red";
});
